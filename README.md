# Start

On host:
```
> vagrant up
> vagrant ssh
```

In box:
```
> npm install
> grunt build:modules
> grunt db:init (if you want to initialize DB with default data)
> grunt server:dev
```

On host go to [http://localhost:8080/](http://localhost:8080/)
