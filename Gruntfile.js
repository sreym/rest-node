var fs = require('fs');
var path = require('path');
var mongoose = require('mongoose');

module.exports = function(grunt) {
  var browserify_targets = function(cwd, src, dest) {
    var res = grunt.file.expand({cwd: cwd}, src).map(function(el) {
      var result = {};
      result[path.join(dest, el)] = path.join(cwd, el);
      return result;
    });
    return res;
  };

  grunt.initConfig({
    concat: {
      bower_js: {
        src: [
          'bower_components/jquery/dist/jquery.min.js',
          'bower_components/bootstrap/dist/js/bootstrap.min.js',
          'bower_components/react/react.min.js',
          'bower_components/react/JSXTransformer.js',
          'bower_components/es6-promise/promise.min.js',
          'bower_components/fetch/fetch.js'
        ],
        dest: '_build/public/js/basic.js'
      },
      bower_css: {
        src: ['bower_components/bootstrap/dist/css/bootstrap.min.css', 'bower_components/bootstrap/dist/css/bootstrap-theme.min.css'],
        dest: '_build/public/css/basic.css'
      }
    },
    copy: {
      assets: {files: [{expand: true, cwd: 'assets', src: '**', dest: '_build', isFile: true}]},
      js: {files: [{expand: true, cwd: 'src', src: '**/*.js', dest: '_build', isFile: true}]},
      css: {files: [{expand: true, cwd: 'src', src: '**/*.css', dest: '_build', isFile: true}]},
      node_modules: {files: [{expand: true, src: ['node_modules/**', '!node_modules/grunt*/**' ], dest: '_build'}]},
      public_assets: {files: [{expand: true, cwd: 'assets', src: ['public/**','**/*.jade'], dest: '_build', isFile: true}]}
    },
    build: {
      dev: ['jshint', 'copy:assets', 'copy:js', 'copy:css', 'browserify'],
      modules: ['run:npm_install', 'run:bower_install', 'copy:node_modules', 'concat']
    },
    react: {
      src: {
        files: [
          {
            expand: true,
            cwd: 'src',
            src: ['**/*.react.js'],
            dest: '_build',
            ext: '.react.js'
          }
        ]
      }
    },
    express: {
      dev: {
        options: {
          script: '_build/bin/www',
          port: 3000
        }
      }
    },
    watch: {
      assets: {
        files:  [ 'assets/**/*.jade', 'assets/public/**/*.js', 'assets/public/**/*.css', 'assets/public/**/*.html' ],
        tasks:  [ 'copy:public_assets' ],
        options: {spawn: false, livereload: true}
      },
      react: {
        files:  [
          'src/**/*.rbundle.js',
          'src/**/*.react.js',
          'src/public/js/**/*.js'
        ],
        tasks:  [ 'browserify' ],
        options: {spawn: false, livereload: true}
      }
    },
    run: {
      npm_install: {
        cmd: "npm",
        args: ['install']
      },
      bower_install: {
        cmd: "bower",
        args: ['install', '-s']
      }
    },
    server: {
      dev: ['build:dev', 'express:dev', 'watch']
    },
    db: {
      init: {}
    },
    jshint: {
      options: {
        reporter: require('jshint-stylish')
      },
      target: ['src/**/*.js', '!src/**/*.rbundle.js', '!src/**/*.react.js']
    },
    browserify: {
      jsx: {
        files: browserify_targets('src/public/','**/*.rbundle.js', '_build/public'),
        options: {
          transform: ['reactify']
        }
      },
      js: {
        files: browserify_targets('src/public/','**/*.bundle.js', '_build/public')
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-react');
  grunt.loadNpmTasks('grunt-express-server');
  grunt.loadNpmTasks('grunt-run');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-browserify');

  grunt.registerMultiTask('build', function() {
    switch(this.target) {
      default:
        grunt.task.run(this.data);
    }
  });

  grunt.registerMultiTask('db', function() {
    var done = this.async();
    var config = require('./src/config');
    var _this = this;
    var deploydb = require('./src/config/deploydb');
    mongoose.connect(config.db.uri, config.db.options, function() {
      fs.readdirSync(__dirname + '/src/models').forEach(function (file) {
        if (~file.indexOf('.js')) require(__dirname + '/src/models/' + file);
      });

      deploydb[_this.target](mongoose, done);
    });
  });


  grunt.registerMultiTask('server', function() {
    grunt.task.run(this.data);
  });

};
