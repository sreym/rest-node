#!/usr/bin/env bash

apt-get remove node
apt-get update
apt-get upgrade -y
apt-get install -y build-essential mc git
apt-get install -y nodejs npm mongodb
ln -s /usr/bin/nodejs /usr/bin/node
npm install -g bower
npm install -g grunt
npm install -g grunt-cli
npm install -g browserify
apt-get autoremove
apt-get clean

echo "cd /vagrant" >> /home/vagrant/.profile