var React = require('react');

var EditableList = require('./components/EditableList.react');

React.render(
  <EditableList />,
  document.getElementById('example')
);
