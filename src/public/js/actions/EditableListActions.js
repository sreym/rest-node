var AppDispatcher = require('../dispatcher/AppDispatcher');
var EditableListConstants = require('../constants/EditableListConstants');

var EditableListActions = {
  add: function(text) {
    AppDispatcher.dispatch({
      actionType: EditableListConstants.LIST_ADD,
      text: text
    });
  }
};

module.exports = EditableListActions;