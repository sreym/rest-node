var AppDispatcher = require('../dispatcher/AppDispatcher');
var EditableListConstants = require('../constants/EditableListConstants');
var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');

var _list = [];

var CHANGE_EVENT = 'change';

var EditableListStore = assign({}, EventEmitter.prototype, {
  getAll: function() {
    return _list;
  },
  emitChange: function() {
    this.emit(CHANGE_EVENT);
  },
  addChangeListener: function(callback) {
    this.on(CHANGE_EVENT, callback);
  },
  removeChangeListener: function(callback) {
    this.removeListener(CHANGE_EVENT, callback);
  }
});

AppDispatcher.register(function(action) {
  var text;
  switch (action.actionType) {
    case EditableListConstants.LIST_ADD:
      text = action.text.trim();
      _list.push(text);
      EditableListStore.emitChange();
      break;
    default:
      // no actions
  }

});

module.exports = EditableListStore;