var React = require('react');

var EditableListStore = require('./../stores/EditableListStore');
var EditableListActions = require('./../actions/EditableListActions');

var inputValue = '';

var EditableList = React.createClass({
  generateState: function() {
    return {
      list: EditableListStore.getAll()
    };
  },
  getInitialState: function() {
    return this.generateState();
  },
  _onChange: function() {
    this.setState(this.generateState());
  },
  _onChangeInput: function(e) {
    inputValue = e.target.value;
  },
  componentDidMount: function() {
    EditableListStore.addChangeListener(this._onChange);
  },
  componentWillUnmount: function() {
    EditableListStore.removeChangeListener(this._onChange);
  },
  add: function() {
    EditableListActions.add(inputValue);
  },
  render: function() {
    var listItems = [];
    for(var i = 0; i < this.state.list.length; i++) {
      listItems.push(<li>{this.state.list[i]}</li>)
    }

    return (
      <div>
        <ol>{listItems}</ol>
        <input onChange={this._onChangeInput}/><button onClick={this.add}>add</button>
      </div>
    );
  }
});

module.exports = EditableList;