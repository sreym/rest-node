var express = require('express');
var router = express.Router();

var controller = require('./../controllers/auth');
var passport = controller.passport;

router.get('/login', controller.loginPage);
router.get('/logout', controller.logoutPage);
router.post('/login',
  passport.authenticate('local', {
    successRedirect: '/',
    failureRedirect: '/login',
    failureFlash: true
  })
);

module.exports = router;