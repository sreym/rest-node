module.exports = {
  init: function(mongoose, done) {
    var UserModel = mongoose.model("User");
    UserModel.remove({}).exec(function() {
      console.log("Database has been cleaned.");
      var user = new UserModel({email: "admin@admin", password: "12345"});
      user.save(function() {
        console.log("User admin@admin with password 12345 has been created.");
        done();
      });
    });
  }
};