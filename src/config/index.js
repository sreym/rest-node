module.exports = {
  db: {
    uri: 'mongodb://localhost/node-rest',
    options: {server: {socketOptions: {keepAlive: 1}}}
  },
  session: {
    secret: 'SUPER SECRET SECRET'
  }
};