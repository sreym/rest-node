var express = require('express');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var flash = require('connect-flash');
var fs = require('fs');
var passport = require('passport');
var session = require('express-session');

var config = require('./config');
var app = express();

// Connect to mongodb
var connect_db = function () {
  mongoose.connect(config.db.uri, config.db.options);
};
connect_db();
mongoose.connection.on('error', console.log);
mongoose.connection.on('disconnected', connect_db);
// require models
fs.readdirSync(__dirname + '/models').forEach(function (file) {
  if (~file.indexOf('.js')) require(__dirname + '/models/' + file);
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(express.static(path.join(__dirname, 'public')));
app.use(logger('dev'));
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

var MongoStore = require('connect-mongo')(session);

app.use(session({
  secret: config.session.secret,
  saveUninitialized: true,
  resave: false,
  store: new MongoStore({ mongooseConnection: mongoose.createConnection(config.db.uri, config.db.options) })
}));
app.use(flash());
app.use(passport.initialize());
app.use(passport.session());

app.use(require('./controllers/session'));

app.use('/', require('./routes/index'));
app.use('/', require('./routes/auth'));

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
